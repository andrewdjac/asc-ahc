The directory "hw1" contains solutions to the first homework assignment.
The directory "hw2" contains solutions to the second homework assignment.
The directory "hw3" contains solutions to the third homework assignment.
The directory "hw4" contains solutions to the fourth homework assignment.
The directory "hw5" contains solutions to the fifth homework assignment.
The directory "hw6" contains solutions to the sixth homework assignment.
The directory "projects" contains solutions to the homework problems from the other three project groups.
