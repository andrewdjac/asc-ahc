// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>
using namespace Rcpp;


// [[Rcpp::export]]
void logliks_Rcpp(int n_in, int m_in, NumericMatrix Y_in, NumericMatrix D_in,
            NumericVector theta_in, int tlen_in, int verb_in, NumericVector llik_out){

}


// [[Rcpp::export]]
double loglik2_Rcpp(NumericMatrix Y, NumericMatrix D, double theta){


	int m = D.nrow();
	arma::mat Sigma(m, m);
	for(int i = 0; i < m; i++){
		for(int j = 0; j < m; j++){
			Sigma(i, j) = exp(-D(i, j) / theta);
		}
	}

	arma::mat Schol = arma::chol(Sigma);
	double ldet = 0;
	for(int i = 0; i < m; i++){
		ldet += log(Schol(i, i));
	}
	
	ldet = 2*ldet;
	arma::mat Scholi = arma::inv(Schol);
	arma::mat Si = Scholi * Scholi.t();
	int n = Y.nrow();
	double pi = 3.1415926535897;
	double ll = -0.5*n*(m*log(2*pi) + ldet);

	arma::mat Y_mat = as<arma::mat>(Y);

	arma::mat prd;
	for(int i = 0; i < n; i++){
		prd = (Y_mat.row(i) * Si) * Y_mat.row(i).t();
		ll -= 0.5 * prd(0, 0);
	}

	return(ll);

}
