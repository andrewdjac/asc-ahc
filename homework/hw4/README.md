This folder contains the source file hw4.Rmd, which contains the solutions for homework 4. R files are stored in the R folder. C and C++ scripts are stored in the src folder.

There are two identical versions of the markdown file, "hw4.Rmd" and "hw4_mkl.Rmd". One is meant to be run using regular R, while the other is meant to be run using the MKL version of R.
