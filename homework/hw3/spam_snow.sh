#!/bin/bash
user_input=${1:-4};
if [[ $user_input -lt 1 ]]
then
        echo "Specified number of sockets needs to be a positive integer."
        exit 1
fi

num_cores=$(nproc)
if [[ $num_cores -lt $user_input ]]
then
        echo "Reduced number of specified (or default) sockets to maximum available ($num_cores)."
        let user_input=num_cores
fi

nohup R CMD BATCH "--args num_sockets=$user_input" spam_snow.R "spam_snow.Rout" &
