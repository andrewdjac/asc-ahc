#!/bin/bash
user_input=${1:-4};
if [[ $user_input -lt 1 ]]
then 
	echo "Specified number of cores needs to be a positive integer."
	exit 1
fi

num_cores=$(nproc)
if [[ $num_cores -lt $user_input ]]
then
	echo "Reduced number of specified (or default) cores to maximum available ($num_cores)."
	let user_input=num_cores
fi	

for ((i = 1 ; i <= $user_input ; i++)); do
	nohup R CMD BATCH "--args seed=$i reps=5" spam_mc.R "spam_mc_${i}.Rout" &
done
