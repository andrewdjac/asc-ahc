#!/bin/bash

#SBATCH -N 5
#SBATCH --ntasks-per-node=32
#SBATCH -t 10:00:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

module purge
module load parallel
module load intel/18.2 mkl R/3.6.2

#set r libraries
export R_LIBS="~/Rlib:$R_LIBS"

#set number of cores used by each r process
export MKL_NUM_THREADS=4

#number of r processes to run
ncopies=30

#processes to run at a time
nparallel=6

echo "$( date ): Starting spam_mc"

scontrol show hostname $SLURM_NODELIST > node.list

seq 1 $ncopies | parallel -j $nparallel --sshloginfile node.list --workdir $PWD --env R_LIBS "module purge; module load intel/18.2 mkl R/3.6.2; export MKL_NUM_THREADS=4; R CMD BATCH \"--args seed={} reps=1\" spam_mc.R spam_mc_{}.Rout"

echo "$( date ): Finished spam_mc"


echo "$( date ): Starting spam_mc_collect"
R CMD BATCH spam_mc_collect.R spam_mc_collect.Rout
echo "$( date ): Finished spam_mc_collect"


