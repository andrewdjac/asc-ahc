#!/bin/sh
#SBATCH -N 5
#SBATCH --ntasks-per-node=32
#SBATCH -t 12:00:00
#SBATCH -p normal_q
#SBATCH -A ascclass

### Add modules
module purge
module load intel/18.2 mkl R/3.6.2 openmpi/4.0.1 R-parallel/3.6.2

export MKL_NUM_THREADS=4

# Set r libraries
export R_LIBS="$HOME/cascades/R/3.6.2/intel/18.2/lib:$R_LIBS"

# Set num threads
export OMP_NUM_THREADS=$SLURM_NTASKS_PER_NODE

# OpenMPI environment variables
export OMPI_MCA_btl_openib_allow_ib=1 #allow infiniband
export OMPI_MCA_rmaps_base_inherit=1  #slaves inherit environment

# Run R
SCRIPT=spam_snow.R  

echo "$( date ): Starting spam_snow"

mpirun -np 1 --map-by ppr:8:node --bind-to none Rscript $SCRIPT &

## Solution to the MPI busy-wait problem
while [ 1 ]; do
    sleep 1
    PID=$(pgrep -f "R --slave --no-restore --file=$SCRIPT")
    [ -z "$PID" ] || break
done

renice 19 $PID

wait

echo "$( date ): Finished spam_snow"

exit;
