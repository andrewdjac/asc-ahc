# Advanced Statistical Computing
## Andrew Cooper (ahcooper@vt.edu)

This repository was created by Andrew Cooper, a second-year PhD candidate in the Statistics department at Virginia Tech.
The purpose of this repository is to develop, complete, and submit homework assignments for the Fall 2021 course "Advanced Statistical Computing".

The directory "homework" contains past and current homework solutions. The directory "notes" contains important things to remember throughout the duration of the course.
